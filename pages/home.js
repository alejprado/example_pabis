const assert = require('assert');

const Page = require('../pages/page');

const newPage = Object.create(Page, {
// elements
    logo: { get() { return $('//a[@class="nav-logo"]'); } },
    window: { get() { return $('#root-app'); } },
    inputSearch: { get() { return $('//input[@aria-label="Ingresá lo que quieras encontrar"]'); } },
    buttonSearch: { get() { return $('//button[@class="nav-search-btn"]'); } },
    allItems: { get() { return $('#searchResults').$$('//li[@class="results-item highlighted article stack product "]'); } },
// methods

  goToPage: {
    value(page) {
      this.window.scrollIntoView(0,0)
      $('//a[text()="'+ page + '"]').click()
    },
  },

  searchProduct: {
    value(product) {
      this.inputSearch.setValue(product)
      this.buttonSearch.click();
    },
  },

  virifyItemsAmoutn: {
    value(value) {
      assert(this.allItems.length > value)
    },
  },

  open: {
    value() {
      browser.url('');
      this.inputSearch.waitForDisplayed();
    },
  },

});

module.exports = newPage;
